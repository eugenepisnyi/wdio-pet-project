import reporterConfig from './reporter.config.js';
import jasmineConfig from './jasmine.config.js';
import { setUpRemoteOptions } from './remoteOptions.js';

export default {
    runner: 'local',
    autoCompileOpts: {
        autoCompile: true,
        tsNodeOpts: {
            project: './tsconfig.json',
            transpileOnly: true,
        },
    },
    maxInstances: 2,
    logLevel: 'error',
    baseUrl: 'https://www.saucedemo.com/',
    waitforTimeout: 10000,
    connectionRetryTimeout: 120000,
    connectionRetryCount: 3,
    ...reporterConfig,
    ...jasmineConfig,
    capabilities: [
        {
            browserName: 'chrome',
            'goog:chromeOptions': {
                args: ['--start-maximized', '--disable-extensions'],
            },
        },
    ],
    beforeSession: (config: object, capabilities: object, specs: string[]) => {
        setUpRemoteOptions(config, capabilities, specs);
    },
    specs: ['./specs/**/*.*'],
    exclude: [
        // 'path/to/excluded/files'
    ],
};
