const connection_options = {
    hostname: process.env.SELENOID_HOSTNAME,
    port: 4444,
    path: '/wd/hub',
};

const selenoid_options = (spec: string) => {
    return {
        'selenoid:options': {
            name: spec,
            enableVNC: true,
            screenResolution: '1920x1080x24',
        },
    };
};

export function setUpRemoteOptions(config: object, capabilities: object, specs: string[]) {
    const isRemote = process.argv.includes('--selenoid');

    if (isRemote) {
        Object.assign(config, connection_options);
        Object.assign(capabilities, selenoid_options(specs[0]));
    }
}
