export default {
    framework: 'jasmine',
    jasmineOpts: {
        showColors: true,
        defaultTimeoutInterval: 60000,
        expectationResultHandler: (passed: boolean) => {
            if (!passed) {
                const timestamp = new Date().toISOString().replace(/:/g, '-');
                browser.saveScreenshot(`./reports/html-reports/screenshots/${timestamp}.png`);
            }
        },
    },
};
