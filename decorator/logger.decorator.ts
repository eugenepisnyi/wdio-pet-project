export function logger() {
    /* eslint-disable  @typescript-eslint/no-explicit-any */
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        const originalMethod = descriptor.value;
        /* eslint-disable  @typescript-eslint/no-explicit-any */
        descriptor.value = async function (...args: any[]) {
            const firstArg = args.length !== 0 ? args.slice(0, 1) : '';
            try {
                console.log(`[INFO] Executing ${propertyKey}${firstArg && ' for ' + firstArg} on ${target.constructor.name}`);

                return await originalMethod.apply(this, args);
            } catch (error) {
                console.log(`[ERROR] Can not execute ${propertyKey}${firstArg && ' for ' + firstArg} because:
                - ${error}`);
            }
        };
    };
}
