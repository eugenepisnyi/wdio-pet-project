import LoginPage from '../pages/login.page.js';
import ProductsPage from '../pages/products.page.js';
import errorsMessagesData from '../data/errors.messages.data.js';
import headerData from '../data/header.data.js';
import { glitch_user, locked_user, standard_user } from '../data/users.credentials.data.js';

describe('Login page', () => {
    beforeEach(async () => {
        await LoginPage.open();
        expect(await LoginPage.isOpened()).toBeTrue();
    });

    it('should not login with locked user', async () => {
        await LoginPage.login(locked_user.name, locked_user.password);
        expect(await LoginPage.getErrorMessage()).toBe(errorsMessagesData.locked_user);
    });

    it('should login with performance glitch user', async () => {
        await LoginPage.login(glitch_user.name, glitch_user.password);
        expect(await ProductsPage.isLogoDisplayed()).toBeTrue();
        expect(await ProductsPage.getLogoText()).toBe(headerData.logo_text);
    });

    it('should login with standard user', async () => {
        await LoginPage.login(standard_user.name, standard_user.password);
        expect(await ProductsPage.isLogoDisplayed()).toBeTrue();
        expect(await ProductsPage.getLogoText()).toBe(headerData.logo_text);
    });
});
