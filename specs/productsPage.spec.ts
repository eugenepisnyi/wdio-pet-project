import LoginPage from '../pages/login.page.js';
import { standard_user } from '../data/users.credentials.data.js';
import ProductsPage from '../pages/products.page.js';
import { productsCardData } from '../data/products.data.js';

describe('Product page', () => {
    beforeAll(async () => {
        await LoginPage.open();
        expect(await LoginPage.isOpened()).toBeTrue();
        await LoginPage.login(standard_user.name, standard_user.password);
        expect(await ProductsPage.isLogoDisplayed()).toBeTrue();
    });

    Object.keys(productsCardData).forEach((key, index) => {
        it(`should contains correct ${productsCardData[key].name} product's card`, async () => {
            const productCardElement = await ProductsPage.getProductCardElement(index);
            expect(await ProductsPage.getImgSource(productCardElement)).toBe(productsCardData[key].imgSource);
            expect(await ProductsPage.getProductName(productCardElement)).toBe(productsCardData[key].name);
            expect(await ProductsPage.getProductDescription(productCardElement)).toBe(productsCardData[key].description);
            expect(await ProductsPage.getProductPrice(productCardElement)).toBe(productsCardData[key].price);
            expect(await ProductsPage.isAddCartBtnClickable(productCardElement)).toBeTrue();
            expect(await ProductsPage.getAddCartBtnText(productCardElement)).toBe('Add to cart');
        });
    });
});
