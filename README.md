## Getting started

### Installation

- The node version must be greater than or equal to version 20.11.1
  - You can download the latest version of node [here](https://nodejs.org/en/download)
- Install dependencies: `npm install`

## Configuration

### Set environment variable

Create .env file at the root of the directory with the structure:

```
LOCKED_USER_NAME=user_name
GLITCH_USER_NAME=user_name
STANDARD_USER_NAME=user_name

ROOT_USER_PASSWORD=user_password

SELENOID_HOSTNAME=selenoid_hostname
```

You can get credentials of users from https://www.saucedemo.com/

## Running tests

### CLI options

| Parameter  |  Type   | Description                   |
| :--------- | :-----: | :---------------------------- |
| --selenoid | Boolean | Run tests in headed browsers. |
| --spec     | String  | Run specific test file.       |
| --suite    | String  | Run specific test files.      |

### Spec execution

- Run **`npm test -- --spec=specs/loginPage.spec.ts --selenoid`**
- Run **`npm test -- --suite=full --selenoid`**
