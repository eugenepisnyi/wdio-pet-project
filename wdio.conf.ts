import generalConfig from './configs/general.config.js';

export const config = {
    ...generalConfig,
    suites: {
        full: ['./specs/loginPage.spec.*', './specs/productsPage.spec.*'],
        login: ['./specs/loginPage.spec.*'],
        products: ['./specs/productsPage.spec.*'],
    },
};
