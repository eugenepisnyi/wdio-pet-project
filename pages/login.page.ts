import { browser, $ } from '@wdio/globals';
import { logger } from '../decorator/logger.decorator.js';

class LoginPage {
    get loginForm() {
        return $('.login-box form');
    }
    get inputUsername() {
        return this.loginForm.$('#user-name');
    }
    get inputPassword() {
        return this.loginForm.$('#password');
    }
    get btnSubmit() {
        return this.loginForm.$('#login-button');
    }
    get errorMessage() {
        return this.loginForm.$('.error-message-container h3');
    }

    public open() {
        return browser.url(`/`);
    }

    public async isOpened(): Promise<boolean> {
        await this.loginForm.waitForDisplayed({ timeout: 15000 });

        return await this.loginForm.isDisplayed();
    }

    @logger()
    public async login(username: string, password: string): Promise<void> {
        await this.inputUsername.setValue(username);
        await this.inputPassword.setValue(password);
        await this.btnSubmit.click();
    }

    public async getErrorMessage() {
        return await this.errorMessage.getText();
    }
}

export default new LoginPage();
