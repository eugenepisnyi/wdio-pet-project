import { $, $$ } from '@wdio/globals';
import { logger } from '../decorator/logger.decorator.js';

class ProductsPage {
    get logo() {
        return $('.app_logo');
    }
    get inventoryItems() {
        return $$('.inventory_list .inventory_item');
    }

    public async isLogoDisplayed(): Promise<boolean> {
        await this.logo.waitForDisplayed({ timeout: 15000 });

        return await this.logo.isDisplayed();
    }

    public async getLogoText() {
        return await this.logo.getText();
    }

    @logger()
    public async getProductCardElement(productItem: number) {
        const element = await this.inventoryItems[productItem];

        return element as unknown as WebdriverIO.Element;
    }

    public async getImgSource(productCardElement: WebdriverIO.Element) {
        return await productCardElement.$('.inventory_item_img img').getAttribute('src');
    }

    public async getProductName(productCardElement: WebdriverIO.Element) {
        return await productCardElement.$('.inventory_item_name').getText();
    }

    public async getProductDescription(productCardElement: WebdriverIO.Element) {
        return await productCardElement.$('.inventory_item_desc').getText();
    }

    public async getProductPrice(productCardElement: WebdriverIO.Element) {
        return await productCardElement.$('.inventory_item_price').getText();
    }

    public async isAddCartBtnClickable(productCardElement: WebdriverIO.Element) {
        return await productCardElement.$('button.btn_primary').isClickable();
    }

    public async getAddCartBtnText(productCardElement: WebdriverIO.Element) {
        return await productCardElement.$('button.btn_primary').getText();
    }
}

export default new ProductsPage();
